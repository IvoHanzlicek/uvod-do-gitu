﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringToCamelCase
{
    class Program
    {
        static void Main(string[] args)
        {
            Replacer replacer = new Replacer();
            Console.WriteLine(replacer.CamelCase("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. " +
                "Nulla-non-arcu-lacinia-neque-faucibus-fringilla." +
                "Cum_sociis_natoque_penatibus_et_magnis_dis_parturient_montes,_nascetur_ridiculus_mus._"));
        }
    }
}
